# TSG CLI Tool

The TSG CLI tool is an utility tool that helps to configure and deploy
the TSG components.

For the documentation, visit the [docs folder](../website/docs/tools/cli/README.md)
