# Interoperability

This page will be extended as soon as there are more releases of the related repositories to inidicate which versions can be used together. Currently, only the latest releases of the `Wallet`, `Control Plane`, and `HTTP Data Plane` are expected to be interoperable, given the amount of changes being made to all of the repositories.

# External Specifications

[Dataspace Protocol](https://docs.internationaldataspaces.org/ids-knowledgebase/v/dataspace-protocol)
