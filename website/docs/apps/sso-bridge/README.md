# TSG SSO Bridge

The SSO Bridge is an OAuth2.0 & OpenID connect server implementation. With as aim to include Verifiable Presentation-based login functionality combined with the TSG Mobile Wallet.

It acts as the default authentication server for all management communication between the TSG applications (Control Planes, Data Planes, Wallets).

> _NOTE_: Further documentation follows soon

# Structure

- [Configuration](./configuration.md)
- [Build process](./build-process.md)
