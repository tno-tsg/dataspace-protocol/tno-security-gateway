# TNO Security Gateway documentation

Welcome to the TNO Security Gateway documentation. This folder contains all the documentation that is available for the TNO Security Gateway. It is divided into apps folders for each component, describing the architecture and all of the configurable parameters. There is a seperate section for deployment of all the components.

## Structure

- Apps
  - [Control Plane](./apps/control-plane/)
  - [HTTP Data Plane](./apps/http-data-plane/)
  - [Wallet](./apps/wallet/)
- [Deployment](./deployment/)
- Tools
  - [CLI Tool](./tools/cli/)
