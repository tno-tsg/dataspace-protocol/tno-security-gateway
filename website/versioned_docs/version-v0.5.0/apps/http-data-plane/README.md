# TSG HTTP Data Plane

The HTTP Data Plane is a basic data plane implementation that allows for exposing HTTP-based applications (both servers as clients).

Given the simplicity of the data plane, the architecture documentation will be limited to a single file to remain a concise and readable documentation.

# Structure

- [Architecture](./architecture.md)
- [Configuration](./configuration.md)
- [Build process](./build-process.md)
- [Interoperability](./interoperability.md)
