# Interoperability

Currently, the HTTP data plane is a component with no external interoperability. And the data plane only works with the same application at both sides of the interaction.

Explicit data plane specifications are expected, including a HTTP-based specification. But at this moment no (draft) version are published for this.
