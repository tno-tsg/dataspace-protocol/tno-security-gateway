# TSG Analytics Data Plane

The Analytics Data Plane is a basic data plane implementation that is aimed at use cases that want to do distributed analysis over multiple parties.

> _NOTE_: The Analytics Data Plane is currently not ready to be used in actual demonstrations

> _NOTE_: Further documentation follows soon

# Structure

- [Configuration](./configuration.md)
- [Build process](./build-process.md)
