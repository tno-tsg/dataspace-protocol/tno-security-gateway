export type GrantType =
  | "authorization_code"
  | "refresh_token"
  | "client_credentials"
  | "password";
