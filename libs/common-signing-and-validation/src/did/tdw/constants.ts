export const PLACEHOLDER = "{SCID}";
export const METHOD = "tdw";
export const PROTOCOL = `did:${METHOD}:1`;
export const BASE_CONTEXT = "https://www.w3.org/ns/did/v1";
