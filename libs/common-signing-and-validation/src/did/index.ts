export * from "./did.resolver.js";
export * from "./key/did.key.resolver.strategy.js";
export * from "./tdw/index.js";
export * from "./types.js";
export * from "./web/did.web.resolver.strategy.js";
