import { HttpStatus, Injectable, Logger } from "@nestjs/common";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { InjectRepository } from "@nestjs/typeorm";
import {
  Paginated,
  PaginationOptionsDto,
  ServerConfig
} from "@tsg-dsp/common-api";
import {
  Agreement,
  AgreementDto,
  ContractAgreementMessage,
  ContractAgreementVerificationMessage,
  ContractNegotiation,
  ContractNegotiationEventMessage,
  ContractNegotiationState,
  ContractNegotiationTerminationMessage,
  ContractOfferMessage,
  ContractRequestMessage,
  defaultContext,
  deserialize,
  HashedMessage,
  Multilanguage,
  NegotiationDetail,
  NegotiationEvent,
  NegotiationProcessEvent,
  NegotiationRole,
  Offer
} from "@tsg-dsp/common-dsp";
import {
  NegotiationDetailDto,
  NegotiationStatusDto
} from "@tsg-dsp/common-dtos";
import crypto from "crypto";
import { Repository } from "typeorm";

import { RootConfig } from "../../config.js";
import {
  NegotiationDetailDao,
  NegotiationProcessEventDao
} from "../../model/negotiation.dao.js";
import { AgreementService } from "../../policy/agreement.service.js";
import { DSPError } from "../../utils/errors/error.js";
import { VCAuthService } from "../../vc-auth/vc.auth.service.js";
import { DspClientService } from "../client/client.service.js";
import { DspGateway } from "../client/dsp.gateway.js";
import {
  NegotiationCreatedEvent,
  NegotiationUpdatedEvent
} from "./negotiation.events.js";

@Injectable()
export class NegotiationService {
  constructor(
    private readonly dsp: DspClientService,
    private readonly server: ServerConfig,
    private readonly config: RootConfig,
    private readonly dspGateway: DspGateway,
    private readonly authService: VCAuthService,
    private readonly agreementService: AgreementService,
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(NegotiationDetailDao)
    private readonly negotiationDetailRepository: Repository<NegotiationDetailDao>,
    @InjectRepository(NegotiationProcessEventDao)
    private readonly negotiationProcessEventRepository: Repository<NegotiationProcessEventDao>
  ) {}
  private readonly logger = new Logger(this.constructor.name);

  private readonly providerTransitions: Record<
    ContractNegotiationState,
    ContractNegotiationState[]
  > = {
    [ContractNegotiationState.REQUESTED]: [
      ContractNegotiationState.OFFERED,
      ContractNegotiationState.AGREED,
      ContractNegotiationState.TERMINATED
    ],
    [ContractNegotiationState.OFFERED]: [],
    [ContractNegotiationState.ACCEPTED]: [
      ContractNegotiationState.AGREED,
      ContractNegotiationState.TERMINATED
    ],
    [ContractNegotiationState.AGREED]: [],
    [ContractNegotiationState.VERIFIED]: [
      ContractNegotiationState.FINALIZED,
      ContractNegotiationState.TERMINATED
    ],
    [ContractNegotiationState.FINALIZED]: [],
    [ContractNegotiationState.TERMINATED]: []
  };
  private readonly consumerTransitions: Record<
    ContractNegotiationState,
    ContractNegotiationState[]
  > = {
    [ContractNegotiationState.REQUESTED]: [ContractNegotiationState.TERMINATED],
    [ContractNegotiationState.OFFERED]: [
      ContractNegotiationState.REQUESTED,
      ContractNegotiationState.ACCEPTED,
      ContractNegotiationState.TERMINATED
    ],
    [ContractNegotiationState.ACCEPTED]: [],
    [ContractNegotiationState.AGREED]: [
      ContractNegotiationState.VERIFIED,
      ContractNegotiationState.TERMINATED
    ],
    [ContractNegotiationState.VERIFIED]: [],
    [ContractNegotiationState.FINALIZED]: [],
    [ContractNegotiationState.TERMINATED]: []
  };

  private readonly allowedTransitions: Record<
    "remote" | "local",
    Record<
      NegotiationRole,
      Record<ContractNegotiationState, ContractNegotiationState[]>
    >
  > = {
    remote: {
      provider: this.consumerTransitions,
      consumer: this.providerTransitions
    },
    local: {
      provider: this.providerTransitions,
      consumer: this.consumerTransitions
    }
  };

  private async checkTransition(
    direction: "remote" | "local",
    negotiation: NegotiationDetail,
    to: ContractNegotiationState
  ) {
    if (
      !this.allowedTransitions[direction][negotiation.role][
        negotiation.state
      ].includes(to)
    ) {
      const event: NegotiationProcessEvent = {
        time: new Date(),
        state: to,
        localMessage: `Negotiation with process ID ${negotiation.localId} cannot transition from ${negotiation.state} to ${to}`,
        type: direction
      };
      const eventObj = this.negotiationProcessEventRepository.create(event);
      negotiation.events.push(eventObj);
      await this.negotiationDetailRepository.save(negotiation);
      throw new DSPError(
        `Negotiation with process ID ${negotiation.localId} cannot transition from ${negotiation.state} to ${to}`,
        HttpStatus.BAD_REQUEST
      ).andLog(this.logger, "warn");
    }
  }

  async getNegotiationFromDataSetId(
    datasetId: string,
    remoteParty?: string
  ): Promise<NegotiationDetailDto> {
    const negotiation = await this.negotiationDetailRepository.findOne({
      where: {
        dataSet: datasetId,
        remoteParty: remoteParty,
        state: ContractNegotiationState.FINALIZED
      },
      order: { modifiedDate: "DESC" }
    });
    if (!negotiation) {
      throw new DSPError(
        `No finalized negotiation found for dataset ID ${datasetId}`,
        HttpStatus.NOT_FOUND
      ).andLog(this.logger, "warn");
    }
    return this.obtainNegotiationDto(negotiation);
  }

  async getNegotiations(
    paginationOptions: PaginationOptionsDto
  ): Promise<Paginated<NegotiationStatusDto[]>> {
    const [negotiations, itemCount] =
      await this.negotiationDetailRepository.findAndCount({
        select: {
          localId: true,
          remoteId: true,
          role: true,
          remoteAddress: true,
          remoteParty: true,
          state: true,
          dataSet: true,
          modifiedDate: true
        },
        skip: paginationOptions.skip,
        take: paginationOptions.take,
        order: {
          [paginationOptions.order_by]: paginationOptions.order
        }
      });
    return {
      data: negotiations,
      total: itemCount
    };
  }

  async obtainNegotiationDto(
    negotiation: NegotiationDetailDao
  ): Promise<NegotiationDetailDto> {
    const { offer, agreementDao, events, ...negotiationPlain } = negotiation;
    const eventsToReturn = await Promise.all(
      events.map(async (e) => {
        return {
          ...e,
          reason: e.reason
            ? await Promise.all(e.reason?.map((r) => r.serialize()))
            : undefined,
          verification: e.verification ? e.verification.serialize() : undefined
        };
      })
    );
    return {
      ...negotiationPlain,
      offer: offer ? offer.serialize() : undefined,
      agreement: agreementDao?.agreement,
      events: eventsToReturn.sort((a, b) => {
        return new Date(a.time).valueOf() - new Date(b.time).valueOf();
      })
    };
  }

  async getNegotiationDto(
    processId: string,
    audience?: string
  ): Promise<NegotiationDetailDto> {
    const negotiation = await this.negotiationDetailRepository.findOneBy({
      localId: processId,
      remoteParty: audience
    });
    if (negotiation) {
      return this.obtainNegotiationDto(negotiation);
    } else {
      throw new DSPError(
        `Cannot get negotiation with process ID ${processId} for ${audience}`,
        HttpStatus.NOT_FOUND
      ).andLog(this.logger, "warn");
    }
  }

  async getNegotiation(
    processId: string,
    audience?: string
  ): Promise<NegotiationDetail> {
    const negotiation = await this.negotiationDetailRepository.findOneBy({
      localId: processId,
      remoteParty: audience
    });
    if (negotiation) {
      return new NegotiationDetail({
        ...negotiation,
        events: negotiation.events.sort((a, b) => {
          return new Date(a.time).valueOf() - new Date(b.time).valueOf();
        }),
        agreement: negotiation.agreementDao
          ? await deserialize<Agreement>({
              "@context": defaultContext(),
              ...negotiation.agreementDao?.agreement
            })
          : undefined
      });
    } else {
      throw new DSPError(
        `Cannot get negotiation with process ID ${processId} for ${audience}`,
        HttpStatus.NOT_FOUND
      ).andLog(this.logger, "warn");
    }
  }

  async requestNew(
    offer: Offer,
    dataSet: string,
    remoteAddress: string,
    audience: string
  ): Promise<NegotiationDetail> {
    const consumerPid = `urn:uuid:${crypto.randomUUID()}`;
    offer.target = dataSet;
    offer.assignee = this.config.iam.didId;
    const contractRequestMessage = new ContractRequestMessage({
      consumerPid: consumerPid,
      offer: offer,
      callbackAddress: `${this.server.publicAddress}/negotiations/callbacks/${consumerPid}`
    });

    const contractNegotiationResponse = await this.dsp.requestNegotiation(
      `${remoteAddress}/request`,
      contractRequestMessage,
      audience
    );
    const contractNegotiation = await deserialize<ContractNegotiation>(
      contractNegotiationResponse
    );
    const negotiation: NegotiationDetail = {
      localId: consumerPid,
      remoteId: contractNegotiation.providerPid,
      role: "consumer",
      remoteAddress: `${remoteAddress}/${contractNegotiation.providerPid}`,
      remoteParty: audience,
      state: ContractNegotiationState.REQUESTED,
      dataSet: dataSet,
      offer: offer,
      events: []
    };

    return await this.createNegotiationDetail(negotiation, "local");
  }

  async createNegotiationDetail(
    negotiation: NegotiationDetail,
    type: "local" | "remote"
  ) {
    const event = this.negotiationProcessEventRepository.create({
      time: new Date(),
      state: ContractNegotiationState.REQUESTED,
      type: type
    });
    negotiation.events.push(event);
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:create",
        negotiation.localId
      );
    }
    return negotiation;
  }

  async handleNewRequest(
    requestMessage: ContractRequestMessage,
    remoteParty: string
  ): Promise<ContractNegotiation> {
    const providerPid = `urn:uuid:${crypto.randomUUID()}`;
    const contractNegotiation = new ContractNegotiation({
      id: providerPid,
      consumerPid: requestMessage.consumerPid,
      providerPid: providerPid,
      state: ContractNegotiationState.REQUESTED
    });

    if (requestMessage.offer.target === undefined) {
      throw new DSPError(
        "Target attribute in provided Offer must be set.",
        HttpStatus.BAD_REQUEST
      ).andLog(this.logger, "warn");
    }

    const negotiation: NegotiationDetail = {
      localId: providerPid,
      remoteId: requestMessage.consumerPid,
      role: "provider",
      remoteAddress: requestMessage.callbackAddress,
      remoteParty: remoteParty,
      state: ContractNegotiationState.REQUESTED,
      dataSet: requestMessage.offer.target,
      offer: requestMessage.offer,
      events: []
    };
    await this.createNegotiationDetail(negotiation, "remote");

    if (this.config.runtime?.controlPlaneInteractions !== "manual") {
      this.eventEmitter.emitAsync(
        "negotiation.create",
        new NegotiationCreatedEvent({
          localId: providerPid,
          offer: requestMessage.offer.serialize(),
          datasetId: requestMessage.offer.target,
          remoteParty: remoteParty
        })
      );
    } else {
      this.dspGateway.sendUpdateToClients("negotiation:create", "created");
    }

    return contractNegotiation;
  }

  async requestExisting(
    offer: Offer,
    processId: string
  ): Promise<NegotiationDetail> {
    const negotiation = await this.getNegotiation(processId);
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.REQUESTED
    );
    offer.target = negotiation.dataSet;
    offer.assignee = this.config.iam.didId;
    const contractRequestMessage = new ContractRequestMessage({
      providerPid: negotiation.remoteId,
      consumerPid: negotiation.localId,
      offer: offer,
      callbackAddress: `${this.server.publicAddress}/negotiations/callbacks/${processId}`
    });
    const contractNegotiationResponse = await this.dsp.requestNegotiation(
      `${negotiation.remoteAddress}/request`,
      contractRequestMessage,
      negotiation.remoteParty
    );

    await deserialize<ContractNegotiation>(contractNegotiationResponse);
    negotiation.state = ContractNegotiationState.REQUESTED;
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.REQUESTED,
      type: "local"
    });
    negotiation.offer = offer;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:create",
        negotiation.localId
      );
    }
    return negotiation;
  }

  async handleExistingRequest(
    processId: string,
    requestMessage: ContractRequestMessage,
    audience: string
  ) {
    if (processId !== requestMessage.providerPid) {
      throw new DSPError(
        `Contract negotiation process ID mismatch ${processId} vs ${requestMessage.providerPid}`,
        HttpStatus.BAD_REQUEST
      ).andLog(this.logger, "warn");
    }
    const negotiation = await this.getNegotiation(processId, audience);
    await this.checkTransition(
      "remote",
      negotiation,
      ContractNegotiationState.REQUESTED
    );
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.REQUESTED,
      type: "remote"
    });
    negotiation.state = ContractNegotiationState.REQUESTED;
    negotiation.offer = requestMessage.offer;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return new ContractNegotiation({
      providerPid: negotiation.localId,
      consumerPid: negotiation.remoteId,
      state: negotiation.state
    });
  }

  async offer(
    offer: Offer,
    processId: string,
    _address?: string
  ): Promise<{ status: string }> {
    // TODO: Allow provider initiated negotiations
    const negotiation = await this.getNegotiation(processId);
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.OFFERED
    );
    offer.assigner = this.config.iam.didId;
    offer.assignee = negotiation.remoteParty;
    const contractOfferMessage = new ContractOfferMessage({
      providerPid: negotiation.localId,
      consumerPid: negotiation.remoteId,
      offer: offer,
      callbackAddress: `${this.server.publicAddress}/negotiations/${negotiation.localId}`
    });
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.OFFERED,
      type: "local"
    });

    await this.dsp.negotiationOffer(
      `${negotiation.remoteAddress}/offers`,
      contractOfferMessage,
      negotiation.remoteParty
    );
    negotiation.offer = contractOfferMessage.offer;
    negotiation.state = ContractNegotiationState.OFFERED;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async handleOffer(
    processId: string,
    contractOfferMessage: ContractOfferMessage,
    audience: string
  ): Promise<{ status: string }> {
    // TODO: Allow provider initiated negotiations
    const negotiation = await this.getNegotiation(processId, audience);
    await this.checkTransition(
      "remote",
      negotiation,
      ContractNegotiationState.OFFERED
    );
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.OFFERED,
      type: "remote"
    });
    negotiation.offer = contractOfferMessage.offer;
    negotiation.state = ContractNegotiationState.OFFERED;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async accept(processId: string): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId);
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.ACCEPTED
    );
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.ACCEPTED,
      type: "local"
    });
    const eventMessage = new ContractNegotiationEventMessage({
      providerPid: negotiation.remoteId,
      consumerPid: negotiation.localId,
      eventType: NegotiationEvent.ACCEPTED
    });
    await this.dsp.negotiationEvent(
      `${negotiation.remoteAddress}/events`,
      eventMessage,
      negotiation.remoteParty
    );
    negotiation.state = ContractNegotiationState.ACCEPTED;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async handleEvent(
    processId: string,
    contractNegotiationEventMessage: ContractNegotiationEventMessage,
    audience: string
  ): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId, audience);
    const newState =
      contractNegotiationEventMessage.eventType === NegotiationEvent.ACCEPTED
        ? ContractNegotiationState.ACCEPTED
        : ContractNegotiationState.FINALIZED;
    await this.checkTransition("remote", negotiation, newState);
    if (newState === ContractNegotiationState.FINALIZED) {
      const agreement = await negotiation.agreement?.serialize();
      const remoteHash = contractNegotiationEventMessage.hashedMessage;
      if (
        remoteHash?.["dspace:algorithm"] === "JsonWebSignature2020" ||
        remoteHash?.["dspace:algorithm"] === "DataIntegrityProof"
      ) {
        try {
          const signature = JSON.parse(remoteHash["dspace:digest"]);
          await this.authService.requestSignatureValidation({
            ...agreement,
            proof: signature
          });
        } catch (e) {
          throw new DSPError(
            `Signature validation failed for negotiation ${processId}`,
            HttpStatus.BAD_REQUEST,
            e
          );
        }
      }
      const agreementDao = await this.agreementService.storeAgreement(
        agreement!,
        negotiation.localId,
        negotiation.events.find((e) => e.type === "local" && e.hashedMessage)
          ?.hashedMessage,
        remoteHash
      );
      await this.negotiationDetailRepository.save({
        ...negotiation,
        agreementDao: agreementDao
      });
    }
    negotiation.events.push({
      time: new Date(),
      state: newState,
      type: "remote",
      hashedMessage: contractNegotiationEventMessage.hashedMessage
    });
    negotiation.state = newState;
    await this.negotiationDetailRepository.save(negotiation);

    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async agree(processId: string): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId);
    if (negotiation.offer === undefined) {
      throw new DSPError(
        `No offer present for negotiation ${processId}, no agreement can be created`,
        HttpStatus.BAD_REQUEST
      ).andLog(this.logger, "warn");
    }
    if (negotiation.offer.target === undefined) {
      throw new DSPError(
        `No agreement target available`,
        HttpStatus.INTERNAL_SERVER_ERROR
      ).andLog(this.logger, "warn");
    }
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.AGREED
    );
    const agreement = new Agreement({
      ...negotiation.offer,
      target: negotiation.offer.target,
      timestamp: new Date().toISOString(),
      assignee: negotiation.remoteParty,
      assigner: this.config.iam.didId
    });
    const agreementMessage = new ContractAgreementMessage({
      providerPid: negotiation.localId,
      consumerPid: negotiation.remoteId,
      agreement: agreement
    });
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.AGREED,
      agreementMessage: JSON.stringify(agreementMessage.serialize()),
      type: "local"
    });

    negotiation.state = ContractNegotiationState.AGREED;
    negotiation.agreement = agreement;
    negotiation.agreementId = agreement.id;
    const agreementDao = await this.agreementService.storeAgreement(
      agreement.serialize(),
      negotiation.localId
    );
    await this.negotiationDetailRepository.save({
      ...negotiation,
      agreementDao: {
        id: agreementDao.id
      }
    });
    await this.dsp.negotiationAgreement(
      `${negotiation.remoteAddress}/agreement`,
      agreementMessage,
      negotiation.remoteParty
    );
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }

    return {
      status: "OK"
    };
  }

  async handleAgreement(
    processId: string,
    contractAgreementMessage: ContractAgreementMessage,
    audience: string
  ): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId, audience);
    await this.checkTransition(
      "remote",
      negotiation,
      ContractNegotiationState.AGREED
    );
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.AGREED,
      agreementMessage: JSON.stringify(contractAgreementMessage.serialize()),
      type: "remote"
    });
    negotiation.agreement = contractAgreementMessage.agreement;
    negotiation.agreementId = contractAgreementMessage.agreement.id;
    const agreementDao = await this.agreementService.storeAgreement(
      await contractAgreementMessage.agreement.serialize(),
      negotiation.localId
    );
    negotiation.state = ContractNegotiationState.AGREED;
    await this.negotiationDetailRepository.save({
      ...negotiation,
      agreementDao: agreementDao
    });
    if (this.config.runtime?.controlPlaneInteractions !== "manual") {
      this.eventEmitter.emitAsync(
        "negotiation.agree",
        new NegotiationUpdatedEvent({
          processId: processId
        })
      );
    } else {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }

    return {
      status: "OK"
    };
  }

  async verify(processId: string): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId);
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.VERIFIED
    );
    const contractAgreementMessage = negotiation.events.find(
      (e) => e.agreementMessage !== undefined
    );
    if (contractAgreementMessage === undefined) {
      throw new DSPError(
        `No agreement message present that can be signed for verification`,
        HttpStatus.BAD_REQUEST
      ).andLog(this.logger, "warn");
    }
    let algorithm;
    let digest;
    const agreement = await negotiation.agreement!.serialize();
    try {
      const signedDocument = await this.authService.requestSignature(agreement);
      digest = JSON.stringify(signedDocument["proof"]);
      algorithm = signedDocument["proof"]["type"];
    } catch (e) {
      this.logger.warn(`Could not sign agreement: ${e}`);
      digest = this.createHash(JSON.stringify(agreement));
      algorithm = "sha256";
    }
    const contractAgreementVerificationMessage =
      new ContractAgreementVerificationMessage({
        providerPid: negotiation.remoteId,
        consumerPid: negotiation.localId,
        hashedMessage: {
          "dspace:algorithm": algorithm,
          "dspace:digest": digest
        }
      });
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.VERIFIED,
      verification: contractAgreementVerificationMessage,
      type: "local",
      hashedMessage: {
        "dspace:algorithm": algorithm,
        "dspace:digest": digest
      }
    });

    negotiation.state = ContractNegotiationState.VERIFIED;
    await this.negotiationDetailRepository.save(negotiation);
    await this.dsp.negotiationVerification(
      `${negotiation.remoteAddress}/agreement/verification`,
      contractAgreementVerificationMessage,
      negotiation.remoteParty
    );
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async handleVerification(
    processId: string,
    contractAgreementVerificationMessage: ContractAgreementVerificationMessage,
    audience: string
  ): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId, audience);
    await this.checkTransition(
      "remote",
      negotiation,
      ContractNegotiationState.VERIFIED
    );
    let agreement: AgreementDto | undefined;
    if (
      contractAgreementVerificationMessage.hashedMessage["dspace:algorithm"] ===
        "JsonWebSignature2020" ||
      contractAgreementVerificationMessage.hashedMessage["dspace:algorithm"] ===
        "DataIntegrityProof"
    ) {
      try {
        const signature = JSON.parse(
          contractAgreementVerificationMessage.hashedMessage["dspace:digest"]
        );
        agreement = await negotiation.agreement?.serialize();
        await this.authService.requestSignatureValidation({
          ...agreement,
          proof: signature
        });
      } catch (e) {
        throw new DSPError(
          `Signature validation failed for negotiation ${processId}`,
          HttpStatus.BAD_REQUEST,
          e
        );
      }
    }
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.VERIFIED,
      verification: contractAgreementVerificationMessage,
      hashedMessage: contractAgreementVerificationMessage.hashedMessage,
      type: "remote"
    });
    negotiation.state = ContractNegotiationState.VERIFIED;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions !== "manual") {
      this.eventEmitter.emitAsync(
        "negotiation.verify",
        new NegotiationUpdatedEvent({
          processId: processId
        })
      );
    } else {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }

    return {
      status: "OK"
    };
  }

  async finalize(processId: string): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId);
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.FINALIZED
    );
    let algorithm;
    let digest;
    const agreement = await negotiation.agreement!.serialize();
    try {
      const signedDocument = await this.authService.requestSignature(agreement);
      digest = JSON.stringify(signedDocument["proof"]);
      algorithm = signedDocument["proof"]["type"];
    } catch (e) {
      this.logger.warn(`Could not sign agreement: ${e}`);
      digest = this.createHash(JSON.stringify(agreement));
      algorithm = "sha256";
    }
    const hashedMessage: HashedMessage = {
      "dspace:algorithm": algorithm,
      "dspace:digest": digest
    };
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.FINALIZED,
      hashedMessage: hashedMessage,
      type: "local"
    });
    const eventMessage = new ContractNegotiationEventMessage({
      providerPid: negotiation.localId,
      consumerPid: negotiation.remoteId,
      hashedMessage: hashedMessage,
      eventType: NegotiationEvent.FINALIZED
    });
    await this.dsp.negotiationEvent(
      `${negotiation.remoteAddress}/events`,
      eventMessage,
      negotiation.remoteParty
    );
    negotiation.state = ContractNegotiationState.FINALIZED;
    const agreementDao = await this.agreementService.storeAgreement(
      agreement!,
      negotiation.localId,
      hashedMessage,
      negotiation.events.find((e) => e.type === "remote" && e.hashedMessage)
        ?.hashedMessage
    );
    await this.negotiationDetailRepository.save({
      ...negotiation,
      agreementDao: agreementDao
    });
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async terminate(
    processId: string,
    code?: string,
    reason?: string
  ): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId);
    await this.checkTransition(
      "local",
      negotiation,
      ContractNegotiationState.TERMINATED
    );
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.TERMINATED,
      code: code,
      reason: reason ? [new Multilanguage(reason)] : undefined,
      type: "local"
    });
    negotiation.state = ContractNegotiationState.TERMINATED;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  async handleTermination(
    processId: string,
    contractNegotiationTerminationMessage: ContractNegotiationTerminationMessage,
    audience: string
  ): Promise<{ status: string }> {
    const negotiation = await this.getNegotiation(processId, audience);
    await this.checkTransition(
      "remote",
      negotiation,
      ContractNegotiationState.TERMINATED
    );
    negotiation.events.push({
      time: new Date(),
      state: ContractNegotiationState.TERMINATED,
      code: contractNegotiationTerminationMessage.code,
      reason: contractNegotiationTerminationMessage.reason,
      type: "remote"
    });
    negotiation.state = ContractNegotiationState.TERMINATED;
    await this.negotiationDetailRepository.save(negotiation);
    if (this.config.runtime?.controlPlaneInteractions === "manual") {
      this.dspGateway.sendUpdateToClients(
        "negotiation:update",
        negotiation.localId
      );
    }
    return {
      status: "OK"
    };
  }

  private createHash(message: string): string {
    const hash = crypto.createHash("sha256");
    hash.update(message);
    return hash.digest().toString("base64");
  }
}
