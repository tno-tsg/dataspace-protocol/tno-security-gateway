import { Body, Controller, Headers, Post } from "@nestjs/common";
import { ApiBody, ApiOkResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { DisableOAuthGuard, validationPipe } from "@tsg-dsp/common-api";
import {
  PresentationQueryMessage,
  PresentationResponseMessage
} from "@tsg-dsp/common-dtos";
import { ApiForbiddenResponseDefault } from "@tsg-dsp/common-dtos";

import { DCPHolderService } from "./holder.service.js";

@Controller("dcp/presentations")
@ApiTags("Presentation DCP")
@DisableOAuthGuard()
export class DCPHolderController {
  constructor(private readonly holderService: DCPHolderService) {}

  @Post("query")
  @ApiOperation({
    summary: "Retrieve presentation",
    description:
      "Request a presentation with a SIOP-token and a PresentationQueryMessage"
  })
  @ApiBody({ type: () => PresentationQueryMessage })
  @ApiOkResponse({ type: () => PresentationResponseMessage })
  @ApiForbiddenResponseDefault()
  async presentationQuery(
    @Body(validationPipe)
    presentationQueryMessage: PresentationQueryMessage,
    @Headers("Authorization") authorizationHeader: string
  ): Promise<PresentationResponseMessage> {
    return await this.holderService.presentationQuery(
      presentationQueryMessage,
      authorizationHeader
    );
  }
}
