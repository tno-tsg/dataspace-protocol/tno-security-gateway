# Wallet API

This app contains the logic for the wallet API. The TSG wallet is an implementation of an SSI wallet using Open ID for Verifiable Credential Issuance and Open ID for Verifiable Presentations. For more information, please visit the `docs` folder.
