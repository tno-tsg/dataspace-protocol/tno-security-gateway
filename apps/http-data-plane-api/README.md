# Http Data Plane API

This app contains the logic for the http data plane. This is the main data plane for the TSG components. It can serve as a proxy between a Control Plane and your own backend service. For more information, please visit the `docs` folder. The intended way to use this is with an OpenAPI specification. This ensures interoperability between the two parties.
