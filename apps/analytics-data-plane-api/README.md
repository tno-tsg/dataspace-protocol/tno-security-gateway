# TSG Analytics Data Plane

This app contains the backend for the TSG Analytics Data Plane. The goal for this app is to provide an initial test environment for data analytics by allowing a more free-form way for handling metadata.

Currently, the data plane does not execute any transfers. Only the metadata cataloging is part of the initial version.

It is expected to have support for different scenarios and topologies for interaction between data planes, especially to research how the topologies can be setup and how the contract negotiation can be handled in such scenarios.
