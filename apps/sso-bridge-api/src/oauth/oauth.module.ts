import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { ClientsModule } from "../clients/clients.module.js";
import { KeyDao } from "../model/keys.dao.js";
import { TokenDao } from "../model/token.dao.js";
import { UsersModule } from "../users/users.module.js";
import { MetadataController } from "./metadata.controller.js";
import { OauthController } from "./oauth.controller.js";
import { OauthService } from "./oauth.service.js";
import { TokenService } from "./token.service.js";

@Module({
  imports: [
    TypeOrmModule.forFeature([TokenDao, KeyDao]),
    UsersModule,
    ClientsModule
  ],
  providers: [OauthService, TokenService],
  controllers: [OauthController, MetadataController],
  exports: [OauthService]
})
export class OauthModule {}
